
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Simplex {
  public static class CustomReader {
    private byte[] data;
    public int start = 0;
    public int end = 0;

    public CustomReader(String fileName) throws IOException {
      // Faster than `BufferedReader`
      var file = new File(fileName);
      var fileBytes = Files.readAllBytes(file.toPath());
      this.data = fileBytes;
    }

    public void readDate() {
      start = ++end;
      if (end >= data.length)
        return;
      while (data[end] != ',') {
        end++;
        if (end >= data.length)
          return;
      }
    }

    public void readValue() {
      var count = 0;
      while (true) {
        if (data[end] == ',') count++;
        end++;
        if (count == 6) break;
      }
      start = end;
      while (data[end] != '\n')
        end++;
      return;
    }

    public void skipHeader() {
      while (data[end++] != '\n');
    }

    public int compareDates(String second) {
      // Faster than parsing and comparing `Date`s
      for (int i = 0; i < second.length(); i++) {
        var c1 = data[i + start];
        var c2 = second.charAt(i);
        if (c1 > c2) return 1;
        if (c1 < c2) return -1;
      }
      return 0;
    }

    private static double[] powers = { 1, 10, 100, 1000, 10000, 100000 };
    private static double[] powers2 = { 1, 0.1, 0.01, 0.001, 0.0001, 0.00001 };

    public double parseDouble() {
      // Faster than `Double.parseDouble`
      var res = 0d;
      var pos = start;
      for (; pos < end; pos++)
        if (data[pos] == '.') break;

      for (int i = start; i < end; i++) {
        if (i < pos)
          res += (data[i] - '0') * powers[pos - i - 1];
        else if (i == pos)
          continue;
        else if (i < pos + 4)
          res += (data[i] - '0') * powers2[i - pos];
        else
          break;
      }

      return res;
    }
  }

  public static class CsvIterator {
    private CustomReader reader;
    private String startDate;
    private String endDate;
    private double next = -1d;

    public CsvIterator(String file, String startDate, String endDate) throws IOException {
      this.reader = new CustomReader(file);
      this.startDate = startDate;
      this.endDate = endDate;
      reader.skipHeader();
    }

    public boolean hasNext() {
      try {
        if (next == -1) return findFirst();
        reader.readDate();
        if (reader.end >= reader.data.length) return false;
        if (reader.compareDates(endDate) < 0) {
          reader.readValue();
          next = reader.parseDouble();
          return true;
        }
//        reader.close();
        return false;
      } catch (Exception e) {
        throw e;
      }
    }

    private boolean findFirst() {
      while (true) {
        reader.readDate();
        if (reader.compareDates(startDate) >= 0) {
          reader.readValue();
          next = reader.parseDouble();
          return true;
        } else {
          reader.readValue();
        }
      }
    }

    public double next() {
      return next;
    }

    public double readValue() {
      reader.readDate();
      reader.readValue();
      next = reader.parseDouble();
      return next;
    }

    public boolean canRead() {
      return reader.end < reader.data.length;
    }
  }

  public static class MatrixBuilder {
    private static int DEFAULT_SIZE = 500;

    private double[][] matrix;
    private String[] files = new String[DEFAULT_SIZE];
    private int numberOfStocks;
    private int range;

    public MatrixBuilder(String pathsFile, String startDate, String endDate) throws IOException {
      var reader = new BufferedReader(new FileReader(pathsFile));
      var line = reader.readLine();
      var matrix = new double[DEFAULT_SIZE][754];
      var i = 0;
      while (line != null) {
        files[i] = line;
        var iterator = new CsvIterator(line, startDate, endDate);
        var prev = -1d;
        var j = 0;
        while (iterator.hasNext()) {
          if (prev == -1) {
            prev = iterator.next();
            continue;
          }
          var current = iterator.next;
          var stockReturn = (current - prev) / current;
          matrix[i][j] = stockReturn;
          prev = current;
          j++;
        }
        if (range > 4 && iterator.canRead()) {
          var current = iterator.readValue();
          var stockReturn = (current - prev) / current;
          matrix[i][j] = stockReturn;
          j++;
        }
        range = j;
        line = reader.readLine();
        i++;
      }
      numberOfStocks = i;

      this.matrix = matrix;
      reader.close();
    }

    public double[][] buildStandardMatrix(double d) {
      var height = numberOfStocks + 2; // n + 1 constraints + maximizing fn
      var width = (numberOfStocks * 2) + 2; // n variables + (n + 1) slack + constant

      var M = -1d; // we need this for the `artificial` variable
      var standardTableau = new double[height][width];

      var deviations = new double[numberOfStocks];
      var deviationsSum = 0d;

      for (var i = 0; i < numberOfStocks; i++) {
        // method for computing variance:
        //   http://jonisalonen.com/2013/deriving-welfords-method-for-computing-variance/
        var sum = 0d;
        var m = 0d;
        var mean = 0d;
        for (var j = 0; j < range; j++) {
          var el = matrix[i][j];
          mean += el;
          var oldM = m;
          m += (el - m) / (j + 1);
          sum += (el - m) * (el - oldM);
        }

        var length = (double) range;
        var variance = sum / (length - 1);
        mean /= length;

        var deviation = Math.sqrt(variance);
        deviationsSum += deviation;
        deviations[i] = deviation;

        standardTableau[height - 1][i] = M - mean;
        standardTableau[height - 2][i] = 1;
      }

      var denominator = 0d;
      for (var i = 0; i < numberOfStocks; i++) {
        denominator += deviationsSum / deviations[i];
      }
      for (var i = 0; i < numberOfStocks; i++) {
        var constraint = (deviationsSum / deviations[i]) / denominator;
        standardTableau[i][width - 1] = d * constraint;
      }
      for (var i = 0; i < height - 2; i++) {
        for (var j = 0; j < numberOfStocks; j++) {
          standardTableau[i][j] = j == i ? 1 : 0;
          standardTableau[i][j + numberOfStocks] = j == i ? 1 : 0;
        }
      }
      standardTableau[height - 2][width - 1] = 1;
      standardTableau[height - 2][width - 2] = 1;
      standardTableau[height - 1][width - 2] = 1;
      return standardTableau;
    }

    public String name(int i) {
      var splitted = files[i].split("/");
      return splitted[splitted.length - 1].split("\\.")[0];
    }
  }

  public static class CsvReader {
    /**
     * Alternative way of reading files (slower than `CustomReader` & `CsvIterator`)
     */
    private static DateFormat format = new SimpleDateFormat("yyyy-MM-dd");

    private String file;
    private char delimiter;
    private CSV stock;
    private double[] cache;

    public CsvReader(String file, char delimiter) throws IOException {
      this.file = file;
      this.delimiter = delimiter;
      parse();
      cache = new double[size()];
    }

    public Date dateAt(int row) {
      var rowString = getStock(row);
      return parseDate(rowString[0]);
    }

    public static Date parseDate(String date) {
      try {
        return format.parse(date);
      } catch (ParseException e) {
        return null;
      }
    }

    public String name() {
      var splitted = file.split("/");
      return splitted[splitted.length - 1].split("\\.")[0];
    }

    private double value(int row) {
      if (row >= cache.length) row = cache.length - 1;
      if (cache[row] != 0) return cache[row];
      var rowString = getStock(row);
      var parsed = parseDouble(rowString[rowString.length - 1]);
      cache[row] = parsed;
      return parsed;
    }

    public double parseDouble(String s) {
      var res = 0d;
      var pos = s.indexOf('.');

      for (int i = 0; i < s.length(); i++) {
        if (i < pos)
          res += (s.charAt(i) - '0') * Math.pow(10, pos - i - 1);
        else if (i == pos)
          continue;
        else if (i < pos + 4)
          res += (s.charAt(i) - '0') * Math.pow(10, -(i - pos));
      }
      return res;
    }

    private void parse() throws IOException {
      var reader = new BufferedReader(new FileReader(file));
      reader.readLine();
      var line = reader.readLine();
      var content = new ArrayList<String[]>(10000);
      while (line != null) {
        var splitted = splitString(line, delimiter);
        content.add(splitted);
        line = reader.readLine();
      }
      this.stock = new CSV(content);
    }

    private static String[] splitString(String s, char delimiter) {
      var result = new String[2];
      // be careful, this will only work for this types of csv
      var pos1 = s.indexOf(delimiter);
      var pos2 = s.lastIndexOf(delimiter);
      result[0] = s.substring(0, pos1);
      result[1] = s.substring(pos2 + 1);
      return result;
    }

    public String[] getStock(int index) {
      return stock.content.get(index);
    }

    public int size() {
      return stock.content.size();
    }

    public static class CSV {
      private List<String[]> content;

      public CSV(List<String[]> content) {
        this.content = content;
      }
    }
  }

  public static class InputReader implements Iterable<CsvReader> {
    private String pathFile;
    private List<CsvReader> stocks;

    public InputReader(String pathFile) throws IOException {
      this.pathFile = pathFile;
      parse();
    }

    private void parse() throws IOException {
      var csvs = Files.lines(Paths.get(pathFile))
          .map(InputReader::parseCsv)
          .collect(Collectors.toList());
      this.stocks = csvs;
    }

    private static CsvReader parseCsv(String file) {
      try {
        return new CsvReader(file, ',');
      } catch (IOException e) {
        return null;
      }
    }

    public int starting(String date) {
      for (int i = 0; i < stocks.get(0).size(); i++) {
        var dateAtIndex = stocks.get(0).getStock(i)[0];
        if (compareDates(dateAtIndex, date) >= 0) {
          return i;
        }
      }
      return -1;
    }

    public int ending(String date) {
      var lastIndex = stocks.get(0).size() - 1;
      for (int i = lastIndex; i >= 0; i--) {
        var dateAtIndex = stocks.get(0).getStock(i)[0];
        var compare = compareDates(dateAtIndex, date);
        if (compare <= 0) {
          if (i == lastIndex)
            return i + 1;
          return i;
        }
      }
      return -1;
    }

    public int compareDates(String first, String second) {
      for (int i = 0; i < second.length(); i++) {
        var c1 = first.charAt(i);
        var c2 = second.charAt(i);
        if (c1 == '-') continue;
        if (c1 > c2) return 1;
        if (c1 < c2) return -1;
      }
      return 0;
    }

    @Override
    public Iterator<CsvReader> iterator() {
      return stocks.iterator();
    }
  }

  public static class SimplexSolver {
    /**
     * With the help of:
     *  https://www.youtube.com/watch?v=woJAb5EgjtI
     * and
     *  https://cbom.atozmath.com/CBOM/Simplex.aspx
     */

    private double[][] tableau;
    private int iteration = 0;

    public SimplexSolver(double[][] tableau) {
      this.tableau = tableau;
    }

    public double[] optimize() {
      do {
        var pivotColumn = columnWithMinimumElement(tableau.length - 1);
        var pivotRow = findPivotRow(pivotColumn);
        updatePivotRow(pivotRow, pivotColumn);
        updateOtherRows(pivotRow, pivotColumn);
//        nextIteration();
      } while(!isOptimal());
      return solution();
    }

    private boolean isOptimal() {
      var lastRow = tableau[tableau.length - 1];
      for (var element : lastRow) {
        if (element < 0) return false;
      }
      return true;
    }

    private int columnWithMinimumElement(int row) {
      var minimum = Double.MAX_VALUE;
      var index = 0;
      var targetRow = tableau[row];
      for (var i = 0; i < targetRow.length - 1; i++) {
        var value = targetRow[i];
        if (minimum >= value) {
          minimum = value;
          index = i;
        }
      }
      return index;
    }

    private int findPivotRow(int pivotColumn) {
      var minimumRatio = Double.MAX_VALUE;
      var index = -1;
      var lastColumnIndex = tableau.length - 1;
      var lastRow = tableau[0].length - 1;

      for (var i = 0; i < lastColumnIndex; i++) {
        var entry = tableau[i][pivotColumn];
        if (entry == 0) { continue; }

        var rhs = tableau[i][lastRow];
        var ratio = rhs / entry;
        if (ratio >= 0.0 && minimumRatio >= ratio) {
          minimumRatio = ratio;
          index = i;
        }
      }
      return index;
    }

    private void updatePivotRow(int pivotRow, int pivotColumn) {
      var pivotValue = tableau[pivotRow][pivotColumn];
      var targetRow = tableau[pivotRow];
      for (var i = 0; i < targetRow.length; i++) {
        targetRow[i] /= pivotValue;
      }
    }

    private void updateOtherRows(int pivotRow, int pivotColumn) {
      for (var i = 0; i < tableau.length; i++) {
        if (i == pivotRow) continue;

        var multiplier = tableau[i][pivotColumn];
        var length = tableau[i].length;
        for (var j = 0; j < length; j++) {
          tableau[i][j] -= multiplier * tableau[pivotRow][j];
        }
      }
    }

    private void nextIteration() {
      iteration++;
    }

    private double[] solution() {
      var solutionSize = tableau[tableau.length - 1].length - 1;
      var solution = new double[solutionSize];
      for (var i = 0; i < solutionSize; i++) {
        var pivotIndex = -1;
        for (var j = 0; j < tableau.length; j++) {
          if (tableau[j][i] > 0) {
            if (pivotIndex >= 0) {
              pivotIndex = -1;
              break;
            }
            pivotIndex = j;
          }
        }
        if (pivotIndex > -1) {
          solution[i] = tableau[pivotIndex][tableau[pivotIndex].length - 1];
        }
      }
      return solution;
    }

    @Override
    public String toString() {
      var sb = new StringBuilder();
      sb.append("Iteration: ");
      sb.append(iteration);
      sb.append("\n");
      for (var aTableau : tableau) {
        for (var j = 0; j < aTableau.length; j++) {
          sb.append(String.format("%.8f", aTableau[j]));
          sb.append(", ");
        }
        sb.append("\n");
      }
      return sb.toString();
    }

    public void print() {
//      System.out.println(toString());
    }
  }

  private static double[][] generateReturnMatrix(InputReader stocks, String start, String end) {
    var startIndex = stocks.starting(start);
    var endIndex = stocks.ending(end);
    var range = endIndex - startIndex;
    var matrix = new double[range][stocks.stocks.size()];
    for (var i = 1; i <= range; i++) {
      var j = 0;
      for (CsvReader stock : stocks) {
        var prev = stock.value(i + startIndex - 1);
        var current = stock.value(i + startIndex);
        var stock_return = (current - prev) / current;
        matrix[i - 1][j] = stock_return;
        j++;
      }
    }
    return matrix;
  }

  private static double[][] buildStandardMatrix(double[][] matrix, double d) {
    var numberOfStocks = matrix[0].length;

    var height = numberOfStocks + 2; // n + 1 constraints + maximizing fn
    var width = (numberOfStocks * 2) + 2; // n variables + (n + 1) slack + constant

    var M = -1d; // we need this for the `artificial` variable
    var standardTableau = new double[height][width];

    var deviations = new double[numberOfStocks];
    var deviationsSum = 0d;

    for (var i = 0; i < numberOfStocks; i++) {
      // method for computing variance:
      //   http://jonisalonen.com/2013/deriving-welfords-method-for-computing-variance/
      var sum = 0d;
      var m = 0d;
      var mean = 0d;
      for (var j = 0; j < matrix.length; j++) {
        var el = matrix[j][i];
        mean += el;
        var oldM = m;
        m += (el - m) / (j + 1);
        sum += (el - m) * (el - oldM);
      }

      var length = (double) matrix.length;
      var variance = sum / (length - 1);
      mean /= length;

      var deviation = Math.sqrt(variance);
      deviationsSum += deviation;
      deviations[i] = deviation;

      standardTableau[height - 1][i] = M - mean;
      standardTableau[height - 2][i] = 1;
    }

    var denominator = 0d;
    for (var i = 0; i < numberOfStocks; i++) {
      denominator += deviationsSum / deviations[i];
    }
    for (var i = 0; i < numberOfStocks; i++) {
      var constraint = (deviationsSum / deviations[i]) / denominator;
      standardTableau[i][width - 1] = d * constraint;
    }
    for (var i = 0; i < height - 2; i++) {
      for (var j = 0; j < numberOfStocks; j++) {
        standardTableau[i][j] = j == i ? 1 : 0;
        standardTableau[i][j + numberOfStocks] = j == i ? 1 : 0;
      }
    }
    standardTableau[height - 2][width - 1] = 1;
    standardTableau[height - 2][width - 2] = 1;
    standardTableau[height - 1][width - 2] = 1;
    return standardTableau;
  }

  public static void printMatrix(double[][] matrix) {
    for (var row : matrix) {
      for (var val : row) {
        System.out.format("%f ", val);
      }
      System.out.println();
    }
  }

  public static double dot(double[] x, double[] y) {
    var res = 0.0;
    for (int i = 0; i < Math.min(x.length, y.length); i++) {
      res += x[i] * y[i];
    }
    return res;
  }

  public static void benchmark(int n, String[] args) throws IOException {
    var benchmarks = new double[n];
    for (int i = 0; i < n; i++) {
      var start = System.currentTimeMillis();

      var reader = new InputReader(args[0]);
      var returnMatrix = generateReturnMatrix(
          reader,
          args[2],
          args[3]
      );
      var standardMatrix = buildStandardMatrix(returnMatrix, Double.parseDouble(args[1]));
      var simplex = new SimplexSolver(standardMatrix);
      var solution = simplex.optimize();
      var stringBuilder = new StringBuilder();
      for (int j = 0; j < reader.stocks.size(); j++) {
        var name = reader.stocks.get(j).name();
        stringBuilder.append(name);
        stringBuilder.append(": ");
        stringBuilder.append(solution[j]);
        if (j < reader.stocks.size() - 1) stringBuilder.append(";");
      }
      benchmarks[i] = System.currentTimeMillis() - start;
    }
    var avg = 0d;
    for (var i : benchmarks) avg += i;
    avg /= n;

    System.out.printf("%.5f\n", avg);
  }

  public static void benchmark2(int n, String[] args) throws IOException {
    var benchmarks = new double[n];
    var benchmarks2 = new double[n];
    for (int i = 0; i < n; i++) {
      var start = System.currentTimeMillis();

      var start2 = System.currentTimeMillis();
      var builder = new MatrixBuilder(args[0], args[2], args[3]);
      benchmarks2[i] = System.currentTimeMillis() - start2;

      var standardMatrix = builder.buildStandardMatrix(Double.parseDouble(args[1]));

      var simplex = new SimplexSolver(standardMatrix);
      var solution = simplex.optimize();

      var stringBuilder = new StringBuilder();
      for (int j = 0; j < builder.numberOfStocks; j++) {
        var name = builder.name(j);
        stringBuilder.append(name);
        stringBuilder.append(": ");
        stringBuilder.append(solution[j]);
        if (j < builder.numberOfStocks - 1) stringBuilder.append(";");
      }
      stringBuilder.toString();

      benchmarks[i] = System.currentTimeMillis() - start;
    }
    var avg = 0d;
    for (var i : benchmarks) avg += i;
    avg /= (double) n;

    var avg2 = 0d;
    for (var i : benchmarks2) avg2 += i;
    avg2 /= (double) n;

    System.out.printf("%.1fms %.1fms\n", avg, avg2);
  }

  public static void submission(String[] args) throws IOException {
    // 1. read data
    var reader = new InputReader(args[0]);

    // 2. prepare simplex tableau
    var returnMatrix = generateReturnMatrix(reader, args[2], args[3]);
    var standardMatrix = buildStandardMatrix(returnMatrix, Double.parseDouble(args[1]));

    // 3. optimize
    var simplex = new SimplexSolver(standardMatrix);
    var solution = simplex.optimize();

    // 4. print solution
    var builder = new StringBuilder();
    for (int i = 0; i < reader.stocks.size(); i++) {
      var name = reader.stocks.get(i).name();
      builder.append(name);
      builder.append(": ");
      builder.append(solution[i]);
      if (i < reader.stocks.size() - 1) builder.append(";");
    }
    System.out.println(builder.toString());
  }

  public static void submission2(String[] args) throws IOException {
    // 1. read data
    var builder = new MatrixBuilder(args[0], args[2], args[3]);

    // 2. prepare simplex tableau
    var standardMatrix = builder.buildStandardMatrix(Double.parseDouble(args[1]));
    // 3. optimize
    var simplex = new SimplexSolver(standardMatrix);
    var solution = simplex.optimize();

    // 4. print solution
    var stringBuilder = new StringBuilder();
    for (int j = 0; j < builder.numberOfStocks; j++) {
      var name = builder.name(j);
      stringBuilder.append(name);
      stringBuilder.append(": ");
      stringBuilder.append(solution[j]);
      if (j < builder.numberOfStocks - 1) stringBuilder.append(";");
    }
    System.out.println(stringBuilder.toString());
  }

  public static void main(String[] args) throws IOException {
//    submission(args);
    submission2(args);
//    benchmark(100, args);
//    benchmark2(100, args);
  }
}
